import static org.junit.jupiter.api.Assertions.*;

class StringsTest {
    //construct a strings object and a string for the tests
    Strings t = new Strings();
    String s = "Smith";
    String g = "My name is tony";

    @org.junit.jupiter.api.Test
    void length() {
        assertTrue(t.length(s) == s.length());
        assertFalse(t.length(s) != s.length());
    }

    @org.junit.jupiter.api.Test
    void charAt() {
        assertTrue(t.charAt(s,3) == s.charAt(3));
        assertFalse(t.charAt(s,3) != s.charAt(3));
    }

    @org.junit.jupiter.api.Test
    void indexOf() {
        assertTrue(t.indexOf(s,'t')==s.indexOf('t'));
        assertFalse(t.indexOf(s,'t')!=s.indexOf('t'));
    }

    @org.junit.jupiter.api.Test
    void subString() {
       assertTrue(t.subString(g,3) == g.substring(3));
       assertFalse(t.subString(g,3) != g.substring(3));
    }
}