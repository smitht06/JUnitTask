

public class Strings {

    //all methods mimic the length, charAt, indexOf and subString methods in java.lang.String
    public int length(String a) {
        return a.length();
    }
    public char charAt(String b, int d) {
        return b.charAt(d);
    }
    public int indexOf(String a, char c){
        return a.indexOf(c);
    }
    public String subString(String a, int b){
        return a.substring(b);
    }



}
